(function($) {

Drupal.behaviors.bo_gallery_admin = {
  attach: function (context) {
    // fancybox
    if ($.fn.fancybox) {
      $("a.gallery-fancybox").fancybox({
        overlayColor: '#000',
        overlayOpacity: 0.8
      });
    }

    $('form a.gallery-deselect').click(function(){
      $('#edit-instance-widget-settings-formatters option').removeAttr('selected');
      return false;
    });
  }
};

})(jQuery);
