<?php
/**
 * @file
 * This file contains wysiwyg integration for the bo_gallery module.
 */


/**
 * Implements hook_wysiwyg_include_directory().
 */
function bo_gallery_wysiwyg_include_directory($type) {
  switch ($type) {
    case 'plugins':
      return 'wysiwyg/plugins';

  }
}


/**
 * Implements hook_filter_info().
 */
function bo_gallery_filter_info() {
  $filters['bo_gallery_filter'] = array(
    'title' => t('Converts Gallery tags to Markup'),
    'description' => t('This filter will convert [[{type:bo_gallery... ]] tags into markup.'),
    'process callback' => 'bo_gallery_filter',
    'weight' => 2,
    'tips callback' => 'bo_gallery_filter_tips',
  );

  // If the WYSIWYG module is enabled, add additional help.
  if (module_exists('wysiwyg')) {
    $filters['bo_gallery_filter']['description'] .= ' ' . t('This must be enabled for the WYSIWYG integration to work correctly with this text filter.');
  }

  return $filters;
}


/**
 * Filter callback for media markup filter.
 */
function bo_gallery_filter($text) {
  $text = ' ' . $text . ' ';
  $text = preg_replace_callback("/\[\[bo_gallery:([0-9]+)\]\]/s", 'bo_gallery_token_to_markup', $text);
  return $text;
}

/**
 * Replace callback to convert tag into markup.
 *
 * @param string $match
 *   Takes a match of tag code
 * @param bool $wysiwyg
 *   Set to TRUE if called from within the WYSIWYG text area editor.
 */
function bo_gallery_token_to_markup($match, $wysiwyg = FALSE) {
  $settings = array();
  $match = str_replace("[[", "", $match);
  $match = str_replace("]]", "", $match);
  $tag = $match[0];

  try {
    if (!is_string($tag)) {
      throw new Exception('Unable to find matching tag');
    }

    $gid = str_replace('bo_gallery:', '', $tag);

    if (empty($gid)) {
      throw new Exception('No Gallery Id');
    }

    $gallery = bo_gallery_load($gid);
    if (!$gallery) {
      throw new Exception('Could not load gallery object');
    }

    if ($wysiwyg) {
      $settings['wysiwyg'] = $wysiwyg;
    }
  }
  catch (Exception $e) {
    watchdog('bo_gallery', 'Unable to render gallery from %tag. Error: %error', array('%tag' => $tag, '%error' => $e->getMessage()));
    return '';
  }

  $element = bo_gallery_get_gallery($gallery, $settings);
  return drupal_render($element);
}


/**
 * Returns a drupal_render() array for the gallery.
 */
function bo_gallery_get_gallery($gallery, $settings = array()) {
  $element = bo_gallery_page_view($gallery);

  return $element;
}


/**
 * Menu callback to load a gallery browsing.
 */
function _bo_gallery_dashboard() {
  // Execute the view and grab the content.
  $view = views_get_view('bo_gallery_browser');
  $view->set_display('default');
  $content = $view->execute_display('default');
  $build = array(
    '#theme' => 'bo_gallery_browser',
    '#content' => $content,
  );

  // Render the page.
  $var = array('content' => drupal_render($build));
  drupal_json_output($var);

}


/**
 * Process variables for bo-gallery.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $gallery
 * - $view_mode
 * - $page
 *
 * @see gallery.tpl.php
 */
function template_preprocess_bo_gallery_browser(&$variables) {
  $variables['styles']  = $variables['elements']['#styles'];
  $variables['scripts']  = $variables['elements']['#scripts'];
  $variables['content']  = $variables['elements']['#content'];
}
