<?php
/**
 * @file
 * This template is used to print a single field in a view. It is not
 * actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the
 * template is perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
?>
<?php 
if (isset($row->field_field_gallery_logo) && !empty($row->field_field_gallery_logo)) :
  print $output;
else:
  $query = db_select('bo_gallery_item', 'gi');
  $query
     ->addJoin('INNER', 'field_data_field_gallery', 'g', 'g.entity_id = gi.giid');
  $query
    ->fields('gi', array('giid'))
    ->orderBy('gi.created', 'ASC')
    ->condition('gi.status', 1)
    ->condition('g.field_gallery_target_id', $row->gid)
    ->range(0, 1);

  if ($gallery_item = bo_gallery_item_load($query->execute()->fetchField())) :
    $gallery_image = $gallery_item->field_gallery_item_image[LANGUAGE_NONE][0];
    print theme('image_style', array(
      'style_name' => 'gallery_wysiwyg_thumbnail',
      'path' => $gallery_image['uri'],
      'alt' => $gallery_image['alt'],
      'title' => $gallery_image['title'],
    ));
  endif;
endif;
?>
