<?php

/**
 * @file
 * Define the WYSIWYG browser plugin.
 */

/**
 * Implements hook_wysiwyg_plugin().
 */
function bo_gallery_gallery_plugin() {
  _bo_gallery_add_settings();

  // Add jquery_ui so we can have a nice popup window.
  if (module_exists('jquery_ui')) {
    jquery_ui_add('ui.dialog');
  }

  // Add custom jquery_ui CSS.
  drupal_add_css(drupal_get_path('module', 'bo_gallery') . '/wysiwyg/jquery_ui.css');

  $plugins['bo_gallery'] = array(
    'title' => t('Gallery'),
    'icon path' => drupal_get_path('module', 'bo_gallery') . '/wysiwyg/images',
    'icon file' => 'bo-gallery.png',
    'icon title' => t('Gallery'),
    'js path' => drupal_get_path('module', 'bo_gallery') . '/wysiwyg',
    'js file' => 'wysiwyg-gallery.js',
    'css path' => drupal_get_path('module', 'bo_gallery') . '/wysiwyg',
    'css file' => 'wysiwyg-gallery.css',
    'settings' => array('path' => drupal_get_path('module', 'bo_gallery') . '/wysiwyg'),
  );

  return $plugins;
}

/**
 * Add Drupal js settings.
 */
function _bo_gallery_add_settings() {
  drupal_add_js(array('bo_gallery' => array('browserURL' => base_path() . 'bo_gallery/browser')), 'setting');
  drupal_add_js(array('bo_gallery' => array('pluginSettings' => array('path' => drupal_get_path('module', 'bo_gallery') . '/wysiwyg'))), 'setting');
}
