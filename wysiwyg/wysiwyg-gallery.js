(function($) {

  /**
   * Open jQuery UI dialog box.
   */
  bo_gallery_dialog_open = function(instanceId) {
    Drupal.settings.bo_gallery.instanceId = instanceId;
    $('body').append('<div id="bo_gallery_dialog"><div id="bo_gallery_dialog_contents" width="572" height="422" /></div>');
    bo_gallery_view_load();
    $("#bo_gallery_dialog").dialog({
      modal: true,
      width: 602,
      height: 484,
      title: 'Gallery Browser',
      beforeclose: function(event, ui) {
        bo_gallery_dialog_close();
      }
    }).show();
  }

  /**
   * Close jQuery UI dialog box.
   */
  bo_gallery_dialog_close = function() {
    $("#bo_gallery_dialog").remove();
  }

  bo_gallery_view_load = function() {
    $.ajax({
      type: "GET",
      url: Drupal.settings.bo_gallery.browserURL,
      success: function(data) {
        $('#bo_gallery_dialog_contents').html(data.content);

        $('a.wysiwyg-gallery').click(function(e) {
          bo_gallery_insert($(this).attr('href').replace('#',''));
          e.preventDefault();
        });
      }
    });
  }

  /**
   * Insert the gallery onto the wysiwyg
   */
  bo_gallery_insert = function(gid) {

    // Get wysiwyg editor instance
    var instanceId = parent.Drupal.settings.bo_gallery.instanceId;

    // Prepare the tag
    var settings = Drupal.settings.bo_gallery.pluginSettings;
    var content = '<img id="bo_gallery-' + gid + '" src="' + settings.path + '/images/placeholder.gif" alt="' + Drupal.t('Gallery ') + gid + '" title="' + Drupal.t('Gallery ') + gid + '" class="wysiwyg-gallery wysiwyg-gallery-placeholder" />';

    // Insert tag
    parent.Drupal.wysiwyg.instances[instanceId].insert(content);

    // Close jQuery UI dialog
    bo_gallery_dialog_close();

  }

  /**
   * Wysiwyg plugin button implementation for Awesome plugin.
   */
  Drupal.wysiwyg.plugins.bo_gallery = {

    /**
     * Execute the button.
     *
     * @param data
     *   An object containing data about the current selection:
     *   - format: 'html' when the passed data is HTML content, 'text' when the
     *     passed data is plain-text content.
     *   - node: When 'format' is 'html', the focused DOM element in the editor.
     *   - content: The textual representation of the focused/selected editor
     *     content.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    invoke: function(data, settings, instanceId) {
      bo_gallery_dialog_open(instanceId);
    },

    /**
     * Prepare all plain-text contents of this plugin with HTML representations.
     *
     * Optional; only required for "inline macro tag-processing" plugins.
     *
     * @param content
     *   The plain-text contents of a textarea.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    attach: function(content, settings, instanceId) {
      var tags = content.match(/\[\[bo_gallery:([0-9]+)\]\]/g);
      if (tags) {
        for (var i = 0; i < tags.length; i++) {
          var gid = tags[i].match(/[0-9]+/)[0];
          content = content.replace(tags[i], this._getPlaceholder(gid, settings));
        }
      }
      return content;
    },

    /**
     * Process all HTML placeholders of this plugin with plain-text contents.
     *
     * Optional; only required for "inline macro tag-processing" plugins.
     *
     * @param content
     *   The HTML content string of the editor.
     * @param settings
     *   The plugin settings, as provided in the plugin's PHP include file.
     * @param instanceId
     *   The ID of the current editor instance.
     */
    detach: function(content, settings, instanceId) {
      var $content = $('<div>' + content + '</div>');
      $.each($('img.wysiwyg-gallery', $content), function (i, elem) {
        var gallery = '[[' + $(elem).attr('id').replace('-', ':') + ']]';
        elem.parentNode.insertBefore(document.createTextNode(gallery), elem);
        elem.parentNode.removeChild(elem);
      });
      return $content.html();
    },

    /**
     * Helper function to return a HTML placeholder.
     *
     * The 'drupal-content' CSS class is required for HTML elements in the editor
     * content that shall not trigger any editor's native buttons (such as the
     * image button for this example placeholder markup).
     */
    _getPlaceholder: function (gid, settings) {
      var result = null;

      return result ? result : '<img id="bo_gallery-' + gid + '" src="' + settings.path + '/images/placeholder.gif" alt="' + Drupal.t('Gallery ') + gid + '" title="' + Drupal.t('Gallery ') + gid + '" class="wysiwyg-gallery wysiwyg-gallery-placeholder" />';
    }
  };

})(jQuery);
