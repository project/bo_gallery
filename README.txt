

INTRODUCTION
============

Bouncing Orange Gallery provides entities, formatters and templates to
create full image gallery pages with views integration. 


INSTALLATION
============

 1. Install and configure the dependencies. Check their README files for
    details.
 2. Add fancybox 1.3.4 to your libraries folder (instructions below).
 3. Install and enable the Bouncing Orange Gallery and Gallery Items modules.
 4. Define permissions on admin/people/permissions page
 5. Install and enable any gallery formatter modules required for your project.


INSTALLING FANCYBOX
===================

1. Download the library from 
   http://fancybox.googlecode.com/files/jquery.fancybox-1.3.4.zip. 
2. Extract the files
3. Upload the library by copying the fancybox directory inside the unzipped 
   folder and placing it either inside sites/all/libraries or 
   sites/example.com/libraries if you have a multi-site installation


DEPENDENCIES
============

Bouncing Orange Gallery depends on the following modules:

 *  image
 *  token
 *  entity
 *  entityreference
 *  field_ui
 *  views


CONFIGURATION & USAGE
=====================

Entities
--------

Entities are installed with minimum requirements on fields. This includes:

Gallery Entity

 *  Gallery Type used to display gallery. Field can be configured to
    restrict the types that can be selected on gallery add/edit
 *  Gallery Logo is a logo to be used for gallery preview/link.
    If needed but no present will default to first gallery item

Gallery Item Entity

 *  Gallery is an entity reference to its associated gallery
 *  Image is the actually image for the gallery item

 
Any other fields required for a project can be added as per normal

 *  Please note that all fields for gallery entity are added when installing 
    bo_gallery_item module. this is to avoid issues when trying to disable
    the bo_gallery module as a result of fields being attached to the gallery
    entity. The bo_gallery_item module must be uninstalled before the
    bo_gallery module can be disabled.

The Bouncing Orange Gallery module comes with a gallery_admin image style 
for use in admin pages and gallery_wysiwyg_thumbnail image style for use
with the WYSIWYG BO Gallery browser.

The module contains an admin page for each of the two entities as well as 
the standard add/edit/delete pages. However, from the gallery admin page 
each gallery has a link to a gallery items page for that specific gallery
containing a form to add items and a list of currently assigned items.


Views
-----

The gallery module entities include full views integration. If displaying
full entity display the Gallery formatter will display each gallery using its
chosen gallery type. 

A bo_gallery_browser views is created on install for use with the WYSIWYG
BO Gallery browser.


WYSIWYG
-------

The BO Gallery module contains integration for the WYSIWYG module which can
be assigned to WYSIWYG profiles. There is also a filter which needs to be 
enabled in the text format configuration and must be processed after 'Limit
allowed HTML tags' if text is filtered and before 'Convert Media Tags to 
Markup' to avoid conflicts.


Theme
-----

All galleries can be overridden in the theme using a variety of methods:

 *  All javascript can be overridden in the theme by overriding the javascript 
    function in the custom theme javascript file
 *  All css can be overridden using the custom theme css file
 *  All galleries are displayed using templates which can be overridden in the
    theme. Template variables can be changed by overriding the template
    preprocess function in the theme's template.php


MAINTAINERS
===========

 *  maverick619 <http://drupal.org/user/992824>
 *  jbloomfield <http://drupal.org/user/834002>
