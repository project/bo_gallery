<?php
/**
 * @file
 * bo_gallery.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function bo_gallery_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'bo_gallery_browser';
  $view->description = 'Browser for use with the gallery button in the WYSIWYG';
  $view->tag = 'default';
  $view->base_table = 'bo_gallery';
  $view->human_name = 'bo_gallery_browser';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '16';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Gallery: Gallery ID */
  $handler->display->display_options['fields']['gid']['id'] = 'gid';
  $handler->display->display_options['fields']['gid']['table'] = 'bo_gallery';
  $handler->display->display_options['fields']['gid']['field'] = 'gid';
  $handler->display->display_options['fields']['gid']['label'] = '';
  $handler->display->display_options['fields']['gid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['gid']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['external'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['gid']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['gid']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['gid']['alter']['html'] = 0;
  $handler->display->display_options['fields']['gid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['gid']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['gid']['hide_empty'] = 0;
  $handler->display->display_options['fields']['gid']['empty_zero'] = 0;
  $handler->display->display_options['fields']['gid']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['gid']['format_plural'] = 0;
  /* Field: Gallery: Gallery Logo */
  $handler->display->display_options['fields']['field_gallery_logo']['id'] = 'field_gallery_logo';
  $handler->display->display_options['fields']['field_gallery_logo']['table'] = 'field_data_field_gallery_logo';
  $handler->display->display_options['fields']['field_gallery_logo']['field'] = 'field_gallery_logo';
  $handler->display->display_options['fields']['field_gallery_logo']['label'] = '';
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_gallery_logo']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_gallery_logo']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_gallery_logo']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_gallery_logo']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_gallery_logo']['settings'] = array(
    'image_style' => 'gallery_wysiwyg_thumbnail',
    'image_link' => '',
  );
  $handler->display->display_options['fields']['field_gallery_logo']['field_api_classes'] = 0;
  /* Field: Gallery: Label */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'bo_gallery';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 1;
  $handler->display->display_options['fields']['title']['alter']['path'] = '#[gid]';
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['link_class'] = 'wysiwyg-gallery';
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  /* Sort criterion: Gallery: Created */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'bo_gallery';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  $export[$view->name] = $view;

  return $export;
}
