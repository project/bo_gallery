<?php
/**
 * @file
 * User page callbacks for the bo_gallery_item module.
 */

/**
 * Gallery items type landing page.
 *
 * This callback simply gives us an unordered list
 * of all of the artwork types we have available.
 * Each one is presented as a link to the manage
 * page for that type.
 */
function bo_gallery_item_page_admin($form, $form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return bo_gallery_item_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['gallery_items']));
  }

  $admin_access = user_access('administer galleries');

  $header = array(
    'image' => t('Image'),
    'title' => array('data' => t('Name'), 'field' => 'title'),
    'author' => t('Author'),
    'status' => array('data' => t('Status'), 'field' => 'status'),
    'created' => array(
      'data' => t('Created'),
      'field' => 'created',
      'sort' => 'desc',
    ),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );

  $options = array();
  $destination = drupal_get_destination();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  foreach (bo_gallery_item_operations() as $operation => $array) {
    $operations[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $operations,
    '#default_value' => 'unblock',
  );
  $operations = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $query = db_select('bo_gallery_item', 'gi');
  $query
    ->addJoin('INNER', 'field_data_field_gallery_item_image', 'i', 'i.entity_id = gi.giid');
  $count_query = clone $query;
  $count_query->addExpression('COUNT(gi.giid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('gi', array('giid', 'title', 'created', 'uid', 'status'))
    ->fields('i', array('field_gallery_item_image_fid'))
    ->condition('i.bundle', 'gallery_item', '=')
    ->limit(40)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  foreach ($result as $row) {
    $file = file_load($row->field_gallery_item_image_fid);
    $path = file_create_url($file->uri);
    $image_themed = '<a href="' . $path . '" rel="galleries" class="gallery-fancybox" title="' . $row->title . '">';
    $image_themed .= theme('image_style', array(
      'style_name' => 'gallery_admin',
      'path' => $file->uri,
      'alt' => '',
      'title' => '',
    ));
    $image_themed .= '</a>';

    $user = user_load($row->uid);
    $options[$row->giid] = array(
      'image' => array(
        'data' => $image_themed,
        'class' => 'logo',
      ),
      'title' => $row->title,
      'author' => theme('username', array('account' => $user)),
      'status' => $row->status ? t('published') : t('not published'),
      'created' => format_date($row->created, 'short'),
    );

    // Build a list of all the accessible operations for the current node.
    $operations = array();

    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'gallery-item/' . $row->giid . '/edit',
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'gallery-item/' . $row->giid . '/delete',
      'query' => $destination,
    );
    $options[$row->giid]['operations'] = array();
    // Render an unordered list of operations links.
    $options[$row->giid]['operations'] = array(
      'data' => array(
        '#theme' => 'links__bo_gallery_item_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['gallery_items'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['gallery_items'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager']['#theme'] = 'pager';

  return $form;
}


/**
 * Gallery items for gallery.
 *
 * This callback simply gives us an unordered list
 * of all of the gallery types we have available.
 * Each one is presented as a link to the manage
 * page for that type.
 */
function bo_gallery_item_gallery_page_admin($gallery) {
  global $user;

  $gallery_item = entity_get_controller('gallery_item')->create();
  $gallery_item->hasgallery = TRUE;
  $gallery_item->title = '';
  $gallery_item->status = 1;
  $gallery_item->created = REQUEST_TIME;
  $gallery_item->uid = $user->uid;
  $gallery_item->caption = '';
  $gallery_item->field_gallery = array(
    'und' => array(
      '0' => array(
        'target_id' => $gallery->gid,
      ),
    ),
  );

  $content = '<h2>Add a new Gallery Item</h2>';
  $content .= drupal_render(drupal_get_form('bo_gallery_item_form', $gallery_item));
  $content .= '<h2>Gallery Items</h2>';
  $content .= drupal_render(drupal_get_form('bo_gallery_item_gallery_page_admin_form', $gallery));

  return $content;
}


/**
 * Gallery items for gallery.
 *
 * This callback simply gives us an unordered list
 * of all of the artwork types we have available.
 * Each one is presented as a link to the manage
 * page for that type.
 */
function bo_gallery_item_gallery_page_admin_form($form, $form_state, $gallery) {

  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return bo_gallery_item_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['gallery_items']));
  }

  $admin_access = user_access('administer galleries');

  $header = array(
    'image' => t('Image'),
    'title' => array('data' => t('Name'), 'field' => 'title'),
    'author' => t('Author'),
    'status' => array('data' => t('Status'), 'field' => 'status'),
    'created' => array(
      'data' => t('Created'),
      'field' => 'created',
      'sort' => 'asc',
    ),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );

  $options = array();
  $destination = drupal_get_destination();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  foreach (bo_gallery_item_operations() as $operation => $array) {
    $operations[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $operations,
    '#default_value' => 'unblock',
  );
  $operations = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $query = db_select('bo_gallery_item', 'gi');
  $query
    ->addJoin('INNER', 'field_data_field_gallery', 'g', 'g.entity_id = gi.giid');
  $query
    ->addJoin('INNER', 'field_data_field_gallery_item_image', 'i', 'i.entity_id = gi.giid');
  $count_query = clone $query;
  $count_query->addExpression('COUNT(gi.giid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('gi', array('giid', 'title', 'created', 'uid', 'status'))
    ->fields('i', array('field_gallery_item_image_fid'))
    ->condition('i.bundle', 'gallery_item', '=')
    ->condition('g.bundle', 'gallery_item', '=')
    ->condition('g.field_gallery_target_id', $gallery->gid, '=')
    ->limit(40)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  foreach ($result as $row) {
    $file = file_load($row->field_gallery_item_image_fid);
    $path = file_create_url($file->uri);
    $image_themed = '<a href="' . $path . '" rel="galleries" class="gallery-fancybox" title="' . $row->title . '">';
    $image_themed .= theme('image_style', array(
      'style_name' => 'gallery_admin',
      'path' => $file->uri,
      'alt' => '',
      'title' => '',
    ));
    $image_themed .= '</a>';

    $user = user_load($row->uid);
    $options[$row->giid] = array(
      'image' => array(
        'data' => $image_themed,
        'class' => 'logo',
      ),
      'title' => $row->title,
      'author' => theme('username', array('account' => $user)),
      'status' => $row->status ? t('published') : t('not published'),
      'created' => format_date($row->created, 'short'),
    );

    // Build a list of all the accessible operations for the current node.
    $operations = array();

    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'gallery-item/' . $row->giid . '/edit',
      'query' => $destination,
    );
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'gallery-item/' . $row->giid . '/delete',
      'query' => $destination,
    );
    $options[$row->giid]['operations'] = array();
    // Render an unordered list of operations links.
    $options[$row->giid]['operations'] = array(
      'data' => array(
        '#theme' => 'links__bo_gallery_item_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['gallery_items'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['gallery_items'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager']['#theme'] = 'pager';

  return $form;
}


/**
 * Validate gallery_item_admin_nodes form submissions.
 *
 * Check if any nodes have been selected to perform the chosen
 * 'Update option' on.
 */
function bo_gallery_item_page_admin_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['gallery_items']) || !count(array_filter($form_state['values']['gallery_items']))) {
    form_set_error('', t('No items selectes.'));
  }
}


/**
 * Process gallery_item_admin_nodes form submissions.
 *
 * Execute the chosen 'Update option' on the selected nodes.
 */
function bo_gallery_item_page_admin_submit($form, &$form_state) {
  $operations = bo_gallery_item_operations();
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes.
  $gallery_items = array_filter($form_state['values']['gallery_items']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($gallery_items), $operation['callback arguments']);
    }
    else {
      $args = array($gallery_items);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}

/**
 * Confirm page for gallery item delete.
 */
function bo_gallery_item_multiple_delete_confirm($form, &$form_state, $gallery_items) {
  $form['gallery_items'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  // array_filter returns only elements with TRUE values.
  foreach ($gallery_items as $giid => $value) {
    $title = db_query('SELECT title FROM {bo_gallery_item} WHERE giid = :giid', array(':giid' => $giid))->fetchField();
    $form['gallery_items'][$giid] = array(
      '#type' => 'hidden',
      '#value' => $giid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . "</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'bo_gallery_item_multiple_delete_confirm_submit';
  $confirm_question = format_plural(count($gallery_items),
                                  'Are you sure you want to delete this item?',
                                  'Are you sure you want to delete these items?');
  return confirm_form($form,
                    $confirm_question,
                    'admin/content/galleries/gallery-items', t('This action cannot be undone.'),
                    t('Delete'), t('Cancel'));
}


/**
 * Delete galleries.
 */
function bo_gallery_item_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    bo_gallery_item_delete_multiple(array_keys($form_state['values']['gallery_items']));
    $count = count($form_state['values']['gallery_items']);
    watchdog('bo_gallery_item', 'Deleted @count posts.', array('@count' => $count));
    drupal_set_message(format_plural($count, 'Deleted 1 post.', 'Deleted @count posts.'));
  }
  $form_state['redirect'] = 'admin/content/galleries/gallery-items';
}


/**
 * Implements hook_bo_gallery_item_operations().
 */
function bo_gallery_item_operations() {
  $operations = array(
    'publish' => array(
      'label' => t('Publish selected gallery item(s)'),
      'callback' => 'bo_gallery_item_mass_update',
      'callback arguments' => array('updates' => array('status' => 1)),
    ),
    'unpublish' => array(
      'label' => t('Unpublish selected gallery item(s)'),
      'callback' => 'bo_gallery_item_mass_update',
      'callback arguments' => array('updates' => array('status' => 0)),
    ),
    'delete' => array(
      'label' => t('Delete selected gallery item(s)'),
      'callback' => NULL,
    ),
  );
  return $operations;
}


/**
 * Function description.
 *
 * Make mass update of gallery items, changing all gallery items in
 * the $gallery_items array to update them with the field values in $updates.
 *
 * IMPORTANT NOTE: This function is intended to work when called
 * from a form submit handler. Calling it outgiide of the form submission
 * process may not work correctly.
 *
 * @param array $gallery_items
 *   Array of gallery item giids to update.
 * @param array $updates
 *   Array of key/value pairs with node field titles and the
 *   value to update that field to.
 */
function bo_gallery_item_mass_update($gallery_items, $updates) {
  // We use batch processing to prevent timeout when updating a large number
  // of nodes.
  if (count($gallery_items) > 10) {
    $batch = array(
      'operations' => array(
        array(
          '_bo_gallery_item_mass_update_batch_process', array($nodes, $updates),
        ),
      ),
      'finished' => '_bo_gallery_item_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'bo_gallery_item') . '/bo_gallery_item.pages.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($gallery_items as $giid) {
      _bo_gallery_item_mass_update_helper($giid, $updates);
    }
    drupal_set_message(t('The update has been performes.'));
  }
}

/**
 * Gallery Item Mass Update - helper function.
 */
function _bo_gallery_item_mass_update_helper($giid, $updates) {
  $gallery_item = bo_gallery_item_load($giid, NULL, TRUE);
  // For efficiency manually save the original node before applying any changes.
  $gallery_item->original = clone $gallery_item;
  foreach ($updates as $title => $value) {
    $gallery_item->$title = $value;
  }
  bo_gallery_item_save($gallery_item);
  return $gallery_item;
}


/**
 * Gallery Item Mass Update Batch operation.
 */
function _bo_gallery_item_mass_update_batch_process($gallery_items, $updates, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($gallery_items);
    $context['sandbox']['nodes'] = $gallery_items;
  }

  // Process nodes by groups of 5.
  $count = min(5, count($context['sandbox']['gallery_items']));
  for ($i = 1; $i <= $count; $i++) {
    // For each nid, load the node, reset the values, and save it.
    $giid = array_shift($context['sandbox']['gallery_items']);
    $gallery_item = _bo_gallery_item_mass_update_helper($giid, $updates);

    // Store result for post-processing in the finished callback.
    $context['results'][] = l($gallery_item->title, 'gallery-item/' . $gallery_item->giid);

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reaches.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}


/**
 * Node Mass Update Batch 'finished' callback.
 */
function _bo_gallery_item_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performes.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing giid not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
}


/**
 * Function description.
 *
 * We clean up the gallery item type (bundle) title
 * and then if there is no such gallery item type we
 * return a 404 not found error page. Next we
 * create a new, empty gallery item object, set a
 * page title, and then display a form.
 */
function bo_gallery_item_add() {
  global $user;

  $gallery_item = entity_get_controller('gallery_item')->create();
  $gallery_item->title = '';
  $gallery_item->status = 1;
  $gallery_item->created = REQUEST_TIME;
  $gallery_item->uid = $user->uid;
  $gallery_item->caption = '';
  drupal_set_title(t('Create @title', array('@title' => 'Gallery Item')), PASS_THROUGH);

  return drupal_get_form('bo_gallery_item_form', $gallery_item);
}


/**
 * Create gallery item form.
 */
function bo_gallery_item_form($form, &$form_state, $gallery_item) {
  // Set the id to identify this as an gallery item edit form.
  $form['#id'] = 'bo-gallery-item-form';

  // Save the driver for later, in case we need it.
  $form['#bo_gallery_item'] = $gallery_item;
  $form_state['bo_gallery_item'] = $gallery_item;

  $user = new stdClass();
  if (!empty($gallery_item->uid)) {
    $user = user_load($gallery_item->uid);
  }
  else {
    $user->mail = '';
  }

  $form['options'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('administer riders'),
    '#title' => t('Publishing options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'additional_settings',
    '#attributes' => array(
      'class' => array('node-form-options'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'node') . '/node.js'),
    ),
    '#group' => 'bo_gallery_item_settings',
    '#weight' => -100,
  );
  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $gallery_item->status,
  );

  $form['bo_gallery_item_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 80,
  );

  // Common fields. We don't have many.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $gallery_item->title,
    '#weight' => -5,
    '#required' => TRUE,
  );

  // Common fields. We don't have many.
  $form['caption'] = array(
    '#type' => 'textfield',
    '#title' => t('Caption'),
    '#default_value' => $gallery_item->caption,
    '#weight' => 70,
    '#required' => FALSE,
  );

  $form['uid'] = array('#type' => 'hidden', '#value' => $gallery_item->uid);

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('bo_gallery_item_form_submit'),
  );

  if (!empty($gallery_item->giid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete galleries'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('bo_gallery_item_form_delete_submit'),
    );
  }

  $form['#validate'][] = 'bo_gallery_item_form_validate';

  field_attach_form('gallery_item', $gallery_item, $form, $form_state);

  return $form;
}


/**
 * Gallery item form valdation callback.
 *
 * Tell the Field system to do its own validation.
 */
function bo_gallery_item_form_validate($form, &$form_state) {
  // Field validation.
  entity_form_field_validate('gallery_item', $form, $form_state);
}


/**
 * Gallery item form submit callback.
 */
function bo_gallery_item_form_submit($form, &$form_state) {
  global $user;

  $gallery_item = &$form_state['bo_gallery_item'];
  $account = $form['#bo_gallery_item'];

  $gallery_item->title = $form_state['values']['title'];
  $gallery_item->status = $form_state['values']['status'];
  $gallery_item->uid = $form_state['values']['uid'];
  $gallery_item->caption = $form_state['values']['caption'];
  $gallery_item->path = $form_state['values']['path'];

  // Notify field widgets.
  field_attach_submit('gallery_item', $gallery_item, $form, $form_state);

  // Save the gallery item.
  $succ = bo_gallery_item_save($gallery_item);
  drupal_flush_all_caches();

  if (isset($form['#bo_gallery_item']->hasgallery) && $form['#bo_gallery_item']->hasgallery == TRUE) {
    if ($succ) {
      // Notify the user.
      drupal_set_message(t('Gallery Item saved.'));
    }
    else {
      drupal_set_message(t('Gallery Item could not be saved. Please try again'), 'error');
    }

    $gid = $account->field_gallery[LANGUAGE_NONE][0]['target_id'];
    $form_state['redirect'] = 'admin/content/galleries/' . $gid . '/gallery-items';
  }
  else {
    if ($succ) {
      // Notify the user.
      drupal_set_message(t('Gallery Item saved.'));
      $form_state['redirect'] = 'admin/content/galleries/gallery-items';
    }
    else {
      drupal_set_message(t('Gallery Item could not be saved. Please try again'), 'error');
      $form_state['redirect'] = 'admin/content/galleries/gallery-items/create';
    }
  }
}


/**
 * Function description.
 *
 * Let the controller class do the hard work and
 * save the gallery item.
 */
function bo_gallery_item_save($gallery_item) {
  return entity_get_controller('gallery_item')->save($gallery_item);
}


/**
 * Function description.
 *
 * All this submit handler does is redirect the user
 * to gallery-item/$giid/delete. The rest of the code there
 * is simply to handle Drupal's page redirect system,
 * which we can largely copy and paste.
 */
function bo_gallery_item_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $gallery_item = $form['#bo_gallery_item'];
  $form_state['redirect'] = array('admin/content/galleries/gallery-items/' . $gallery_item->giid . '/delete', array('query' => $destination));
}


/**
 * Function description.
 *
 * Rather than build a complete form, we will simply
 * pass data on to a utility function of the Form API
 * called confirm_form().
 */
function bo_gallery_item_delete_confirm($form, &$form_state, $gallery_item) {
  $form['#bo_gallery_item'] = $gallery_item;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['giid'] = array(
    '#type' => 'value',
    '#value' => $gallery_item->giid,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title?', array('%title' => $gallery_item->title)),
    'gallery-item/' . $gallery_item->giid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


/**
 * Function description.
 *
 * If the user submits the confirmation form, then
 * we know he really means it. The submit callback
 * handles deleting the gallery item.
 */
function bo_gallery_item_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $gallery_item = bo_gallery_item_load($form_state['values']['giid']);
    bo_gallery_item_delete($gallery_item->giid);
    watchdog('bo_gallery_item', '@type: deleted %title.', array('@type' => 'Gallery Item', '%title' => $gallery_item->title));
  }

  drupal_set_message(t('@type %title has been deleted.', array('@type' => 'Gallery Item', '%title' => $gallery_item->title)));

  $form_state['redirect'] = '<front>';
}


/**
 * Gallery item edit page callback.
 */
function bo_gallery_item_page_edit($gallery_item) {
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => 'Gallery Item', '@title' => $gallery_item->title)), PASS_THROUGH);
  return drupal_get_form('bo_gallery_item_form', $gallery_item);
}
