<?php

/**
 * @file
 * Default theme implementation to display a gallery.
 *
 * Available variables:
 * gallery: Complete gallery object
 * gallery_url: URL of the gallery
 * title: Title of the gallery
 * created: Timestamp for gallery creation
 * status: Published status
 * content: Gallery content
 *
 * @see template_preprocess()
 * @see template_preprocess_gallery()
 * @see template_process()
 */
?>
<div class="gallery gallery-<?php print $gallery->gid; ?> <?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  <h3><?php print $title; ?></h3>
  <?php print $content; ?>

</div>
