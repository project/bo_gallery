(function($) {

  // Function to control the scrolling
  //$.fn.scroller = function($item_width, $step_no, $direction){
  $.fn.scroller = function(options){

    this.each(function(){
      // Extend our default options with those provided.
      // Note that the first arg to extend is an empty object -
        // this is to keep from overriding our "defaults" object.
      var opts = $.extend({}, $.fn.scroller.defaults, options);

      // build element specific options
      var o = $.meta ? $.extend({}, opts, $this.data()) : opts;

      var container = $(this);

      var visible = parseInt(o.visible,10);
      var direction = o.direction;
      var inverse = o.inverse;
      var step_no = o.step_no;
      var speed = o.speed;
      var end_button = o.end_button;
      var easing = o.easing;
      var default_item = o.default_item;
      var pager = o.pager;
      var pager_separator = o.pager_separator;

      var button_left = $('.button-left',container);
      var button_right = $('.button-right',container);

      if (end_button == true){
        var button_start = $('.button-start',container);
        var button_end = $('.button-end',container);
      }

      var item_size = 0;
      // Initialise direction and get the size of each item
      if(direction == 'vertical'){
        direction = 'top';
        if (inverse == true){
          $('.scroller-in',container).css({'top':'auto'});
          $('.scroller-in',container).css({'bottom':'0px'});
          direction = 'bottom';
        }
        var height = parseInt($('.scroller-item',container).css('height'),10);

        var ptop = $('.scroller-item',container).css('padding-top');
        if (ptop == 'auto'){ ptop = 0; }

        var pbottom = $('.scroller-item',container).css('padding-bottom');
        if (pbottom == 'auto'){ pbottom = 0; }

        var padding = parseInt(ptop,10) + parseInt(pbottom,10);

        var mtop = $('.scroller-item',container).css('margin-top');
        if (mtop == 'auto'){ mtop = 0; }

        var mbottom = $('.scroller-item',container).css('margin-bottom');
        if (mbottom == 'auto'){ mbottom = 0; }

        var margin = parseInt(mtop,10) + parseInt(mbottom,10);

        var btop = $('.scroller-item',container).css('border-top-width');
        if (btop == 'medium'){ btop = 0; }

        var bbottom = $('.scroller-item',container).css('border-bottom-width');
        if (bbottom == 'medium'){ bbottom = 0; }

        var border = parseInt(btop,10) + parseInt(bbottom,10);

        var item_size = height + padding + margin + border;

      } else if(direction == 'horizontal'){
        direction = 'left';
        if (inverse == true){
          $('.scroller-in',container).css({'left':'auto'});

          $('.scroller-in',container).css({'right':'0px'});
          direction = 'right';
        }

        var width = parseInt($('.scroller-item',container).css('width'),10);

        var pleft = $('.scroller-item',container).css('padding-left');
        if (pleft == 'auto'){ pleft = 0; }

        var pright = $('.scroller-item',container).css('padding-right');
        if (pright == 'auto'){ pright = 0; }

        var padding = parseInt(pleft,10) + parseInt(pright,10);

        var mleft = $('.scroller-item',container).css('margin-left');
        if (mleft == 'auto'){ mleft = 0; }

        var mright = $('.scroller-item',container).css('margin-right');
        if (mright == 'auto'){ mright = 0; }

        var margin = parseInt(mleft,10) + parseInt(mright,10);

        var bleft = $('.scroller-item',container).css('border-left-width');
        if (bleft == 'medium'){ bleft = 0; }

        var bright = $('.scroller-item',container).css('border-right-width');
        if (bright == 'medium'){ bright = 0; }

        var border = parseInt(bleft,10) + parseInt(bright,10);

        var item_size = width + padding + margin + border;
      }

      // Initialise the animate css object
      var t = {};
      t[direction] = 0;

      // If inverse turn off right buttons at the start
      if (inverse == true){
        // Turn off left button to start
        $('span',button_right).addClass('end');
        $('span',button_left).removeClass('end');

        // Turn off start button to start
        if (end_button == true){
          $('span',button_end).addClass('end');
          $('span',button_start).removeClass('end');
        }
      } else {
        // Turn off left button to start
        $('span',button_left).addClass('end');
        $('span',button_right).removeClass('end');

        // Turn off start button to start
        if (end_button == true){
          $('span',button_start).addClass('end');
          $('span',button_end).removeClass('end');
        }
      }

      // Get the number of items in scroller
      var items = $(' .scroller-item',container).length;

      // Get width as negative value to check for end of scroller
      var width = items * parseInt('-' + item_size, 10);

      // Get the last position on the right
      var last = width + (visible * item_size);

      // Get number of pixels to scroll each time
      var step = item_size * step_no;

      // Initialise strting position
      var pos = 0;

      // Set width or height of inner scroller
      var div_width = items * parseInt(item_size, 10);
      if (direction == 'top' || direction == 'bottom'){
        $('.scroller-in',container).css({'height':div_width});
      } else {
        $('.scroller-in',container).css({'width':div_width});
      }

      if (inverse == true){
        // Right Button
        button_right.each(function(){
          var $this = $(this);
          var hd = $('span', $this);
          var bt = $('<a href="#">&nbsp;</a>');

          // Deactivate right button if too few items
          if (items <= visible){
            $('span',button_left).addClass('end');
          }

          bt.click(function(){
            // Only scroll if there are more items than can be viewed
            if (items > visible){
              button = $(this);

              // Only do stuff if its not already scrolling
              if (!button.hasClass('scrolling')){
                // Get current position
                pos = parseInt($('.scroller-in',container).css(direction), 10);
                // Get next position
                var new_pos = pos + step + 'px';
                // If next position is past the start then set new position to the start
                if (parseInt(new_pos, 10) > 0){
                  new_pos = '0px';
                }
                // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                if(pos != new_pos){
                  t[direction] = new_pos;
                  button.addClass('scrolling');
                  $('.scroller-in',container).animate(t,speed,easing,function(){
                    button.removeClass('scrolling');
                  });
                  $('span',button_left).removeClass('end');
                  if (end_button == true){$('span',button_start).removeClass('end');}
                }

                // Check position to change button state
                if (new_pos == '0px'){
                  $('span',button_right).addClass('end');
                  if (end_button == true){$('span',button_end).addClass('end');}
                }
              }
            }
            return false;
          });
          hd.text('');
          bt.prependTo(hd);
        });

        // Left Button
        button_left.each(function(){
          var $this = $(this);
          var hd = $('span', $this);
          var bt = $('<a href="#">&nbsp;</a>');
          $(bt,container).click(function(){

            // Only scroll if there are more items than can be viewed
            if (items > visible){
              button = $(this);
              // Only do stuff if its not already scrolling
              if (!button.hasClass('scrolling')){

                // Get current position
                pos = parseInt($('.scroller-in',container).css(direction), 10);
                // Get next position
                var new_pos = pos - step + 'px';
                // Get position of the next press of this button
                var next_pos = parseInt(new_pos, 10);
                // If next position is past the end then change new position to only go to the end of the last scroller item and not the full step
                if (next_pos <= last){
                  new_pos = last + 'px';
                  next_pos = last;
                  $('span',button_left).addClass('end');
                  if (end_button == true){$('span',button_start).addClass('end');}
                }
                // If the new position is not at the end, scroll and activate left button for use
                if (next_pos >= last){
                  t[direction] = new_pos;
                  button.addClass('scrolling');
                  $('.scroller-in',container).animate(t,speed,easing,function(){
                    button.removeClass('scrolling');
                  });
                  $('span',button_right).removeClass('end');
                  if (end_button == true){$('span',button_end).removeClass('end');}
                }
              }
            }
            return false;
          });
          hd.text('');
          bt.prependTo(hd);
        });

        if (end_button == true){
          // End Button
          button_end.each(function(){
            var $this = $(this);
            var hd = $('span', $this);
            var bt = $('<a href="#">&nbsp;</a>');

            // Deactivate right button if too few items
            if (items <= visible){
              $('span',button_start).addClass('end');
            }

            bt.click(function(){
              // Only scroll if there are more items than can be viewed
              if (items > visible){
                button = $(this);

                // Only do stuff if its not already scrolling
                if (!button.hasClass('scrolling')){
                  // Get current position
                  pos = parseInt($('.scroller-in',container).css(direction), 10);
                  // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                  if(pos != 0){
                    t[direction] = '0px';
                    button.addClass('scrolling');
                    $('.scroller-in',container).animate(t,speed,easing,function(){
                      button.removeClass('scrolling');
                    });
                    $('span',button_left).removeClass('end');
                    $('span',button_start).removeClass('end');
                    $('span',button_right).addClass('end');
                    $('span',button_end).addClass('end');
                  }
                }
              }
              return false;
            });
            hd.text('');
            bt.prependTo(hd);
          });

          // Start Button
          button_start.each(function(){
            var $this = $(this);
            var hd = $('span', $this);
            var bt = $('<a href="#">&nbsp;</a>');

            // Deactivate start button if too few items
            if (items <= visible){
              $('span',button_start).addClass('end');
            }

            bt.click(function(){
              // Only scroll if there are more items than can be viewed
              if (items > visible){
                button = $(this);

                // Only do stuff if its not already scrolling
                if (!button.hasClass('scrolling')){
                  // Get current position
                  pos = parseInt($('.scroller-in',container).css(direction), 10);
                  // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                  if(pos != last){
                    t[direction] = last + 'px';
                    button.addClass('scrolling');
                    $('.scroller-in',container).animate(t,speed,easing,function(){
                      button.removeClass('scrolling');
                    });
                    $('span',button_right).removeClass('end');
                    $('span',button_end).removeClass('end');
                    $('span',button_left).addClass('end');
                    $('span',button_start).addClass('end');
                  }
                }
              }
              return false;
            });
            hd.text('');
            bt.prependTo(hd);
          });
        }

      } else {

        // Left Button
        button_left.each(function(){
          var $this = $(this);
          var hd = $('span', $this);
          var bt = $('<a href="#">&nbsp;</a>');

          // Deactivate right button if too few items
          if (items <= visible){
            $('span',button_right).addClass('end');
          }

          bt.click(function(){
            // Only scroll if there are more items than can be viewed
            if (items > visible){
              button = $(this);

              // Only do stuff if its not already scrolling
              if (!button.hasClass('scrolling')){
                // Get current position
                pos = parseInt($('.scroller-in',container).css(direction), 10);
                // Get next position
                var new_pos = pos + step + 'px';
                var next_pos = parseInt(new_pos, 10);
                // If next position is past the start then set new position to the start
                if (parseInt(new_pos, 10) > 0){
                  new_pos = '0px';
                }
                // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                if(pos != new_pos){
                  t[direction] = new_pos;
                  button.addClass('scrolling');
                  $('.scroller-in',container).animate(t,speed,easing,function(){
                    button.removeClass('scrolling');
                  });
                  $('span',button_right).removeClass('end');
                  if (end_button == true){$('span',button_end).removeClass('end');}
                }

                // Check position to change button state
                if (new_pos == '0px'){
                  $('span',button_left).addClass('end');
                  if (end_button == true){$('span',button_start).addClass('end');}
                }

                if (pager == true){
                  pager_step = item_size * visible * -1;
                  var position = (Math.ceil(next_pos / pager_step)) + 1;
                  if (!$('#scroller-pager-item-' + position).hasClass('active')){
                    $('.scroller-pager-item.active').removeClass('active');
                    $('#scroller-pager-item-' + position).addClass('active');
                  }
                }
              }
            }
            return false;
          });
          hd.text('');
          bt.prependTo(hd);
        });

        // Right Button
        button_right.each(function(){
          var $this = $(this);
          var hd = $('span', $this);
          var bt = $('<a href="#">&nbsp;</a>');
          $(bt,container).click(function(){

            // Only scroll if there are more items than can be viewed
            if (items > visible){
              button = $(this);
              // Only do stuff if its not already scrolling
              if (!button.hasClass('scrolling')){

                // Get current position
                pos = parseInt($('.scroller-in',container).css(direction), 10);
                // Get next position
                var new_pos = pos - step + 'px';
                // Get position of the next press of this button
                var next_pos = parseInt(new_pos, 10);
                // If next position is past the end then change new position to only go to the end of the last scroller item and not the full step
                if (next_pos <= last){
                  new_pos = last + 'px';
                  next_pos = last;
                  $('span',button_right).addClass('end');
                  if (end_button == true){$('span',button_end).addClass('end');}
                }
                // If the new position is not at the end, scroll and activate left button for use
                if (next_pos >= last){
                  t[direction] = new_pos;
                  button.addClass('scrolling');
                  $('.scroller-in',container).animate(t,speed,easing,function(){
                    button.removeClass('scrolling');
                  });
                  $('span',button_left).removeClass('end');
                  if (end_button == true){$('span',button_start).removeClass('end');}
                }

                if (pager == true){
                  pager_step = item_size * visible * -1;
                  var position = (Math.ceil(next_pos / pager_step)) + 1;
                  if (!$('#scroller-pager-item-' + position).hasClass('active')){
                    $('.scroller-pager-item.active').removeClass('active');
                    $('#scroller-pager-item-' + position).addClass('active');
                  }
                }
              }
            }
            return false;
          });
          hd.text('');
          bt.prependTo(hd);
        });

        if (end_button == true){
          // Start Button
          button_start.each(function(){
            var $this = $(this);
            var hd = $('span', $this);
            var bt = $('<a href="#">&nbsp;</a>');

            // Deactivate right button if too few items
            if (items <= visible){
              $('span',button_end).addClass('end');
            }

            bt.click(function(){
              // Only scroll if there are more items than can be viewed
              if (items > visible){
                button = $(this);

                // Only do stuff if its not already scrolling
                if (!button.hasClass('scrolling')){
                  // Get current position
                  pos = parseInt($('.scroller-in',container).css(direction), 10);
                  // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                  if(pos != 0){
                    t[direction] = '0px';
                    button.addClass('scrolling');
                    $('.scroller-in',container).animate(t,speed,easing,function(){
                      button.removeClass('scrolling');
                    });
                    $('span',button_right).removeClass('end');
                    $('span',button_end).removeClass('end');
                    $('span',button_left).addClass('end');
                    $('span',button_start).addClass('end');
                  }

                  if (pager == true){
                    if (!$('#scroller-pager-item-1').hasClass('active')){
                      $('.scroller-pager-item.active').removeClass('active');
                      $('#scroller-pager-item-1').addClass('active');
                    }
                  }
                }
              }
              return false;
            });
            hd.text('');
            bt.prependTo(hd);
          });

          // End Button
          button_end.each(function(){
            var $this = $(this);
            var hd = $('span', $this);
            var bt = $('<a href="#">&nbsp;</a>');

            // Deactivate end button if too few items
            if (items <= visible){
              $('span',button_end).addClass('end');
            }

            bt.click(function(){
              // Only scroll if there are more items than can be viewed
              if (items > visible){
                button = $(this);

                // Only do stuff if its not already scrolling
                if (!button.hasClass('scrolling')){
                  // Get current position
                  pos = parseInt($('.scroller-in',container).css(direction), 10);
                  // If the new position is different to the current position (ie. not the start) scroll and activate right button for use
                  if(pos != last){
                    t[direction] = last + 'px';
                    button.addClass('scrolling');
                    $('.scroller-in',container).animate(t,speed,easing,function(){
                      button.removeClass('scrolling');
                    });
                    $('span',button_left).removeClass('end');
                    $('span',button_start).removeClass('end');
                    $('span',button_right).addClass('end');
                    $('span',button_end).addClass('end');
                  }

                  if (pager == true){
                    var position = Math.ceil(items / visible);
                    if (!$('#scroller-pager-item-' + position).hasClass('active')){
                      $('.scroller-pager-item.active').removeClass('active');
                      $('#scroller-pager-item-' + position).addClass('active');
                    }
                  }
                }
              }
              return false;
            });
            hd.text('');
            bt.prependTo(hd);
          });

        }
      }

      // Pager
      if (pager === true){
        // total pages
        pager_total = Math.ceil(items / visible);
        pager_html = '';
        // Create html for pages
        for (i = 1; i <= pager_total; i++){
          pager_html += '<a href="#" id="scroller-pager-item_' + i + '" class="scroller-pager-item">' + i + '</a>'
          if (i != pager_total){
            pager_html += pager_separator;
          }
        }

        $('.scroller-pager',container).html(pager_html);

        // Scroll pager
        $('.scroller-pager-item').click(function(){
          if (!$(this).hasClass('active')){
            // Change active pager link
            $('.scroller-pager-item.active').removeClass('active');
            $(this).addClass('active');

            // Get position of selected page
            var position = parseInt($(this).text(),10) - 1;
            var new_pos = '-' + (position * (item_size * visible)) + 'px';
            var next_pos = parseInt(new_pos, 10);
            // If the new position is at the end
            if (next_pos <= last){
              new_pos = last + 'px';
              next_pos = last;
              t[direction] = new_pos;
              $('.scroller-in',container).animate(t,speed,easing);
              $('span',button_left).removeClass('end');
              $('span',button_right).addClass('end');
              if (end_button == true){
                $('span',button_start).removeClass('end');
                $('span',button_end).addClass('end');
              }
            }
            // If the new position is not at the end, scroll and activate left button for use
            else if (next_pos >= last && next_pos != 0){
              t[direction] = new_pos;
              $('.scroller-in',container).animate(t,speed,easing);
              $('span',button_left).removeClass('end');
              $('span',button_right).removeClass('end');
              if (end_button == true){
                $('span',button_start).removeClass('end');
                $('span',button_end).removeClass('end');
              }
            }
            // If the new position is at the start
            else if (next_pos == 0){
              t[direction] = new_pos;
              $('.scroller-in',container).animate(t,speed,easing);
              $('span',button_left).addClass('end');
              if (end_button == true){}
              $('span',button_right).removeClass('end');
              if (end_button == true){
                $('span',button_start).addClass('end');
                $('span',button_end).removeClass('end');
              }
            }
          }

          return false;
        });
      } else {
        $('.scroller-pager',container).remove();
      }

      // Default starting point
      if (default_item != 1){
        var new_pos = '-' + ((Math.ceil(default_item / step_no) - 1) * step_no) * item_size + 'px';
        var next_pos = parseInt(new_pos, 10);
        // If there is no scroll
        if (last == 0){
          $('span',button_left).addClass('end');
          if (end_button == true){$('span',button_start).addClass('end');}
          $('span',button_right).addClass('end');
          if (end_button == true){$('span',button_end).addClass('end');}
        }
        // If new position is at the end
        else if (next_pos <= last){
          new_pos = last + 'px';
          next_pos = last;
          t[direction] = new_pos;
          $('.scroller-in',container).animate(t,1);
          $('span',button_left).removeClass('end');
          if (end_button == true){$('span',button_start).removeClass('end');}
          $('span',button_right).addClass('end');
          if (end_button == true){$('span',button_end).addClass('end');}
        }
        // If the new position is not at the end, scroll and activate left button for use
        else if (next_pos >= last && next_pos != 0){
          t[direction] = new_pos;
          $('.scroller-in',container).animate(t,1);
          $('span',button_left).removeClass('end');
          if (end_button == true){$('span',button_start).removeClass('end');}
        }

        // Set active link in pager
        if (pager === true){
          var position = Math.ceil(default_item / visible);
          $('#scroller-pager-item-' + position).addClass('active');
        }
      } else {
        // Set active link in pager
        if (pager === true){
          $('#scroller-pager-item-1').addClass('active');
        }
      }
    });
  }

  // plugin defaults - added as a property on our plugin function
  $.fn.scroller.defaults = {
    speed: 800,
    step_no: 1,
    visible: 1,
    direction: 'horizontal',
    inverse: false,
    end_button: false,
    easing: 'swing',
    default_item: 1,
    pager: false,
    pager_separator: ' | '
  };

// end of closure
})(jQuery);
