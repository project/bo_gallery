(function($) {

Drupal.behaviors.bo_gallery_buttonless_nav = {
  attach: function (context) {
    $('.gallery-buttonless-nav-horizontal').each(function(){
      $gallery = $(this);

      // Initialise variables
      // Padding for small thumb
      var $pt1 = $('.scroller-item:not(.open)', $gallery).css('padding-top');
      // Padding for large thumb
      var $pt2 = $('.scroller-item.open', $gallery).css('padding-top');
      // Width of small thumb
      var $w1 = $('.scroller-item:not(.open)', $gallery).width();
      // Width of large thumb
      var $w2 = $('.scroller-item.open', $gallery).width();
      // Get the number of items in scroller
      var $items = $('.scroller-item', $gallery).length;

      var $width = parseInt($('.scroller-item:not(.open)', $gallery).css('width'),10);

      var $pleft = $('.scroller-item:not(.open)', $gallery).css('padding-left');
      if ($pleft == 'auto'){ $pleft = 0; }
      var $pright = $('.scroller-item:not(.open)', $gallery).css('padding-right');
      if ($pright == 'auto'){ $pright = 0; }
      var $padding = parseInt($pleft,10) + parseInt($pright,10);

      var $mleft = $('.scroller-item:not(.open)', $gallery).css('margin-left');
      if ($mleft == 'auto'){ $mleft = 0; }
      var $mright = $('.scroller-item:not(.open)', $gallery).css('margin-right');
      if ($mright == 'auto'){ $mright = 0; }
      var $margin = parseInt($mleft,10) + parseInt($mright,10);

      var $bleft = $('.scroller-item:not(.open)', $gallery).css('border-left-width');
      if ($bleft == 'medium'){ $bleft = 0; }
      var $bright = $('.scroller-item:not(.open)', $gallery).css('border-right-width');
      if ($bright == 'medium'){ $bright = 0; }
      var $border = parseInt($bleft,10) + parseInt($bright,10);

      var $item_size = $width + $padding + $margin + $border;

      var $visible = Math.floor(parseInt($('.scroller-outer', $gallery).css('width'),10) / $item_size);

      // Horizontal Gallery Scroller
      $('.gallery-buttonless-nav-horizontal .gallery-thumbs .field-item a').click(function(){
        $this = $(this);
        if (!$this.parent().hasClass('open')){
          image_change($pt1, $pt2, $w1, $w2, $item_size, $items, $visible, 'horizontal', $this, $gallery);
        }

        return false;
      });
    });

    $('.gallery-buttonless-nav-vertical').each(function(){
      $gallery = $(this);

      // Initialise variables
      // Padding for small thumb
      var $pt1 = $('.scroller-item:not(.open)', $gallery).css('padding-left');
      // Padding for large thumb
      var $pt2 = $('.scroller-item.open', $gallery).css('padding-left');
      // Width of small thumb
      var $w1 = $('.scroller-item:not(.open)', $gallery).height();
      // Width of large thumb
      var $w2 = $('.scroller-item.open', $gallery).height();
      // Get the number of items in scroller
      var $items = $('.scroller-item', $gallery).length;

      var $height = parseInt($('.scroller-item:not(.open)', $gallery).css('height'),10);

      var $ptop = $('.scroller-item:not(.open)', $gallery).css('padding-top');
      if ($ptop == 'auto'){ $ptop = 0; }
      var $pbottom = $('.scroller-item:not(.open)', $gallery).css('padding-bottom');
      if ($pbottom == 'auto'){ $pbottom = 0; }
      var $padding = parseInt($ptop,10) + parseInt($pbottom,10);

      var $mtop = $('.scroller-item:not(.open)', $gallery).css('margin-top');
      if ($mtop == 'auto'){ $mtop = 0; }
      var $mbottom = $('.scroller-item:not(.open)', $gallery).css('margin-bottom');
      if ($mbottom == 'auto'){ $mbottom = 0; }
      var $margin = parseInt($mtop,10) + parseInt($mbottom,10);

      var $btop = $('.scroller-item:not(.open)', $gallery).css('border-top-width');
      if ($btop == 'medium'){ $btop = 0; }
      var $bbottom = $('.scroller-item:not(.open)', $gallery).css('border-bottom-width');
      if ($bbottom == 'medium'){ $bbottom = 0; }
      var $border = parseInt($btop,10) + parseInt($bbottom,10);

      var $item_size = $height + $padding + $margin + $border;

      var $visible = Math.floor(parseInt($('.scroller-outer', $gallery).css('height'),10) / $item_size);

      // Vertical Gallery Scroller
      $('.gallery-buttonless-nav-vertical .gallery-thumbs .field-item a').click(function(){
        $this = $(this);
        if (!$this.parent().hasClass('open')){
          image_change($pt1, $pt2, $w1, $w2, $item_size, $items, $visible, 'vertical', $this, $gallery);
        }

        return false;
      });
    });
  }

};

/**
 * $pt1: Padding for small thumb
 * $pt2: Padding for large thumb
 * $w1: Width of small thumb
 * $w2: Width of large thumb
 * $item_size:  Width + padding for small thumb container
 */
image_change = function($pt1, $pt2, $w1, $w2, $item_size, $items, $visible, $direction, $link, $context){
  // Animate thumb size
  if ($direction == 'horizontal') {
    $('.open', $context).animate({paddingTop:$pt1, width:$w1 + 'px'}, 100);
    $('.open img', $context).animate({width:$w1 + 'px'},100,function(){
      $(this).parent().parent().removeClass('open');
    });
    $href = $link.attr('href');
    $('.gallery-large img', $context).attr('src', $href);
    if ($link.attr('title') == '') {
      $caption = '&nbsp;';
    } else {
      $caption = $link.attr('title');
    }

    $('.gallery-caption', $gallery).html($caption);
    $('.gallery-large img', $gallery).attr('title', $caption);
    $link.parent().animate({paddingTop:$pt2, width:$w2 + 'px'}, 100);
    $('img',$link).animate({width:$w2 + 'px'},100,function(){
      $(this).parent().parent().addClass('open');
    });
  } else if ($direction == 'vertical') {
    $('.open', $context).animate({paddingLeft:$pt1, height:$w1 + 'px'}, 100);
    $('.open img', $context).animate({height:$w1 + 'px'},100,function(){
      $(this).parent().parent().removeClass('open');
    });
    $href = $link.attr('href');
    $('.gallery-large img', $context).attr('src', $href);
    if ($link.attr('title') == '') {
      $caption = '&nbsp;';
    } else {
      $caption = $link.attr('title');
    }

    $('.gallery-caption', $gallery).html($caption);
    $('.gallery-large img', $gallery).attr('title', $caption);
    $link.parent().animate({paddingLeft:$pt2, height:$w2 + 'px'}, 100);
    $('img',$link).animate({height:$w2 + 'px'},100,function(){
      $(this).parent().parent().addClass('open');
    });
  }

  // Only animate if there are nore images than what is visible
  if ($items > $visible) {
    $id = $link.parent().attr('id').replace('field-item-','');
    // Get width as negative value to check for end of scroller
    var $width = ($items - 1) * parseInt('-' + $item_size, 10) - $w2;

    // Get the last position on the right
    var $last = $width + (($visible - 1) * $item_size) + $w2;

    $pos = ($id - Math.ceil($visible / 2)) * -$item_size;
    if ($pos <= $last){
      $pos = $last + 'px';
    } else if ($pos >= 0){
      $pos = '0px';
    }

    // Animate scroller
    if ($direction == 'horizontal') {
      $('.scroller-in', $context).stop().animate({'left':$pos},200,'swing');
    } else if ($direction == 'vertical') {
      $('.scroller-in', $context).stop().animate({'top':$pos},200,'swing');
    }
  }
};

})(jQuery);
