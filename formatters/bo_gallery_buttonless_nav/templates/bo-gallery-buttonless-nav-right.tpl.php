<?php

/**
 * @file
 * Default theme implementation to display a gallery.
 *
 * Available variables:
 * title: Title of the gallery
 * created: Timestamp for gallery creation
 * status: Published status
 * logo: Gallery logo image (imagecached)
 * gallery_items: Gallery items
 *
 * @see template_preprocess()
 * @see template_preprocess_gallery()
 * @see template_process()
 */
?>
<div class="gallery-content gallery-buttonless gallery-right gallery-buttonless-nav-vertical">
  <div class="gallery-container">
    <div class="gallery-caption">
      <?php print $caption; ?>
    </div>
    <div class='gallery-large'><?php print $logo; ?></div>
  </div>
  
  <div class="gallery-thumbs">
    <?php if (count($gallery_items) >= 2) : ?>
      <div class='scroller-outer'><div class='scroller-in'>
        <?php $i = 1; ?>
        <?php foreach ($gallery_items as $gallery_item) : ?>
          <?php if ($i == 1) :
            $class = 'open';
          else:
            $class = '';
          endif; ?>
          <div id="field-item-<?php print $i; ?>" class="scroller-item field-item <?php print $class; ?>">
            <a href="<?php print $gallery_item->image; ?>" rel="gallery-<?php print $gallery->gid; ?>" title="<?php print $gallery_item->caption; ?>">
              <img src="<?php print $gallery_item->thumb; ?>" alt="" />
            </a>
          </div>
          <?php $i++; ?>
        <?php endforeach; ?>
      </div></div>
    <?php endif; ?>
  </div>
</div>
