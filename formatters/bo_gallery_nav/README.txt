INTRODUCTION
============

The BO Gallery Nav formatter module for the Bouncing Orange Gallery provides 
six formatters to be used to display the gallery. 

Two contain a scroller navigation with gallery items opening in fancybox. One
of the menus is vertical while the other is horizontal.

The other four formatters contain the same navigation with a large image which
changes on thumbnail click. Each formatter places the navigation on a different
side of the large image.


INSTALLATION
============

 1. Install and enable the Bouncing Orange Gallery and Gallery Items modules.
 3. Install and enable bo_gallery_nav formatter module.


DEPENDENCIES
============

Bouncing Orange Gallery depends on the following modules:

 *  bo_gallery_item


CONFIGURATION & USAGE
=====================

Formatters
----------

No configuration is required for the formatters. They are assigned to galleries
on the gallery edit page.
