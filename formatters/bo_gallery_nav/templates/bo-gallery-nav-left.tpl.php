<?php

/**
 * @file
 * Default theme implementation to display a gallery.
 *
 * Available variables:
 * title: Title of the gallery
 * created: Timestamp for gallery creation
 * status: Published status
 * logo: Gallery logo image (imagecached)
 * gallery_items: Gallery items
 *
 * @see template_preprocess()
 * @see template_preprocess_gallery()
 * @see template_process()
 */
?>
<div class="gallery-content gallery-content-large gallery-scroller gallery-nav-left gallery-nav-vertical">
  <div class="gallery-nav">
    <div class="gallery-thumbs">
      <div class="gallery-thumbs-container">
        <div class='button-left'><span></span></div>
        <div class='scroller-outer'><div class='scroller-in'>
        
          <?php foreach ($gallery_items as $gallery_item) : ?>
            <div class="scroller-item">
              <a href="<?php print $gallery_item->image; ?>" title="<?php print $gallery_item->caption; ?>">
                <?php print $gallery_item->thumb; ?>
              </a>
            </div>
          <?php endforeach; ?>
        
        </div></div>
        <div class='button-right'><span></span></div>
      </div>
      <div class="scroller-pager">&nbsp;</div>
    </div>
  </div>
  
  <div class="gallery-container">
    <div class="gallery-large">
      <?php print $logo; ?>
    </div>
    <div class="gallery-caption">
      <?php print $caption; ?>
    </div>
  </div>

</div>
