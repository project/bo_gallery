(function($) {

Drupal.behaviors.bo_gallery_nav = {
  attach: function (context) {
    // Get group out of class and add to rel to avoid validation errors with rel
    $(".gallery a.fancybox").each(function(){
      $class = $(this).attr('class').replace('fancybox ','');
      $(this).attr('rel',$class);
    });

    // fancybox
    if ($.fn.fancybox) {
      $(".gallery-nav a.fancybox").fancybox({
        overlayColor: '#000',
        overlayOpacity: 0.8
      });
    }

    // Scroller
    $('.gallery-nav-horizontal .gallery-thumbs').scroller({
      step_no: 3,
      visible: 3,
      pager: true
    });

    $('.gallery-nav-vertical .gallery-thumbs').scroller({
      step_no: 3,
      visible: 3,
      direction: 'vertical',
      pager: true
    });

    $('.gallery-content-large .scroller-item a').click(function(){
      $this = $(this);
      $gallery = $this.closest('.gallery-content');
      $('.gallery-large img', $gallery).attr('src',$(this).attr('href')).attr('alt',$(this).attr('title'));

      if ($(this).attr('title') == '') {
        $caption = '&nbsp;';
      } else {
        $caption = $(this).attr('title');
      }

      $('.gallery-caption', $gallery).html($caption);
      $('.gallery-large img', $gallery).attr('title', $caption);

      return false;
    });
  }
};

})(jQuery);
