(function($) {

Drupal.behaviors.bo_gallery_single_js = {
  attach: function (context) {
    // Get group out of class and add to rel to avoid validation errors with rel
    $(".gallery a.fancybox").each(function(){
      $class = $(this).attr('class').replace('fancybox ','');
      $(this).attr('rel',$class);
    });

    // fancybox
    if ($.fn.fancybox) {
      $(".gallery a.fancybox").fancybox({
        overlayColor: '#000',
        overlayOpacity: 0.8
      });
    }
  }
};

})(jQuery);
