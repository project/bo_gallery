INTRODUCTION
============

The BO Gallery Simple JS formatter module for the Bouncing Orange Gallery
provides two formatters to be used to display the gallery. 

One simply contains the gallery logo image (or first image in gallery if no
logo is assigned) which, when clicked, opens all gallery items in fancybox.

The second formatter displays all gallery item thumbnails which open in
fancybox when clicked


INSTALLATION
============

 1. Install and enable the Bouncing Orange Gallery and Gallery Items modules.
 3. Install and enable bo_gallery_nav formatter module.


DEPENDENCIES
============

Bouncing Orange Gallery depends on the following modules:

 *  bo_gallery_item


CONFIGURATION & USAGE
=====================

Formatters
----------

No configuration is required for the formatters. They are assigned to galleries
on the gallery edit page.
