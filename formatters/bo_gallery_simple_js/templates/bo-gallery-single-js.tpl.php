<?php

/**
 * @file
 * Default theme implementation to display a gallery.
 *
 * Available variables:
 * title: Title of the gallery
 * created: Timestamp for gallery creation
 * status: Published status
 * logo: Gallery logo image (imagecached)
 * gallery_items: Gallery items
 *
 * @see template_preprocess()
 * @see template_preprocess_gallery()
 * @see template_process()
 */
?>
<div class="gallery-content">
  
  <?php $count = 1; foreach ($gallery_items as $gallery_item) :
    if ($count == 1) : ?>
      <a href="<?php print $gallery_item->path; ?>" class="fancybox gallery-<?php print $gallery->gid; ?>" title="<?php print $gallery_item->caption; ?>"><?php print $logo; ?></a>
    <?php else: ?>
      <a href="<?php print $gallery_item->path; ?>" class="fancybox gallery-<?php print $gallery->gid; ?>" title="<?php print $gallery_item->caption; ?>" style="display: none;">&nbsp;</a>
    <?php endif;
    $count++;
  endforeach; ?>

</div>
