<?php

/**
 * @file
 * Default theme implementation to display a gallery.
 *
 * Available variables:
 * title: Title of the gallery
 * created: Timestamp for gallery creation
 * status: Published status
 * gallery_items: Gallery items
 *
 * @see template_preprocess()
 * @see template_preprocess_gallery()
 * @see template_process()
 */
?>
<div class="gallery-content">
  
  <?php foreach ($gallery_items as $gallery_item) : ?>
    <a href="<?php print $gallery_item->path; ?>" class="fancybox gallery-<?php print $gallery->gid; ?>" title="<?php print $gallery_item->caption; ?>"><?php print $gallery_item->thumb; ?></a>
  <?php endforeach; ?>

</div>
