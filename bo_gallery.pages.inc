<?php
/**
 * @file
 * User page callbacks for the bo_gallery module.
 */

/**
 * Galleries type landing page.
 *
 * This callback simply gives us an unordered list
 * of all of the galleries we have available.
 * Each one is presented as a link to the manage
 * page for that type.
 */
function bo_gallery_galleries_page_admin($form, $form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return bo_gallery_multiple_delete_confirm($form, $form_state, array_filter($form_state['values']['galleries']));
  }

  $admin_access = user_access('administer galleries');

  $header = array(
    'logo' => t('Logo'),
    'title' => array('data' => t('Title'), 'field' => 'title'),
    'author' => t('Author'),
    'status' => array('data' => t('Status'), 'field' => 'status'),
    'created' => array(
      'data' => t('Created'),
      'field' => 'created',
      'sort' => 'desc',
    ),
    'operations' => array('data' => t('Operations'), 'colspan' => 2),
  );

  $options = array();
  $destination = drupal_get_destination();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $admin_access,
  );
  foreach (bo_gallery_operations() as $operation => $array) {
    $operations[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $operations,
    '#default_value' => 'unblock',
  );
  $operations = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $query = db_select('bo_gallery', 'g');
  $query
    ->addJoin('LEFT', 'field_data_field_gallery_logo', 'i', 'i.entity_id = g.gid');
  $count_query = clone $query;
  $count_query->addExpression('COUNT(g.gid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('g', array('gid', 'title', 'created', 'uid', 'status'));
  $query
    ->addfield('i', 'field_gallery_logo_fid', 'logo');
  $query
    ->limit(40)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  foreach ($result as $row) {
    $image_themed = '';
    if (isset($row->logo) && !empty($row->logo)) {
      $file = file_load($row->logo);
      $path = file_create_url($file->uri);
      $image_themed = '<a href="' . $path . '" rel="galleries" class="gallery-fancybox" title="' . $row->title . '">';
      $image_themed .= theme('image_style', array(
        'style_name' => 'gallery_admin',
        'path' => $file->uri,
        'alt' => '',
        'title' => '',
      ));
      $image_themed .= '</a>';
    }

    $user = user_load($row->uid);
    $options[$row->gid] = array(
      'logo' => array(
        'data' => $image_themed,
        'class' => 'logo',
      ),
      'title' => l($row->title, 'gallery/' . $row->gid),
      'author' => theme('username', array('account' => $user)),
      'status' => $row->status ? t('published') : t('not published'),
      'created' => format_date($row->created, 'short'),
    );

    // Build a list of all the accessible operations for the current entity.
    $operations = array();

    // If bo_gallery_item module is enabled.
    if (module_exists('bo_gallery_item')) {
      // Add link to add gallery items.
      $operations['gallery_items'] = array(
        'title' => t('add gallery items'),
        'href' => 'admin/content/galleries/' . $row->gid . '/gallery-items',
      );
      // If there are already gallery items change link text to edit.
      $gallery_items = bo_gallery_item_get_items($row->gid);
      if (!empty($gallery_items)) {
        $operations['gallery_items']['title'] = t('edit gallery items');
      }
    }

    // Edit button.
    $operations['edit'] = array(
      'title' => t('edit'),
      'href' => 'gallery/' . $row->gid . '/edit',
      'query' => $destination,
    );
    // Delete button.
    $operations['delete'] = array(
      'title' => t('delete'),
      'href' => 'gallery/' . $row->gid . '/delete',
      'query' => $destination,
    );
    $options[$row->gid]['operations'] = array();
    // Render an unordered list of operations links.
    $options[$row->gid]['operations'] = array(
      'data' => array(
        '#theme' => 'links__bo_gallery_operations',
        '#links' => $operations,
        '#attributes' => array('class' => array('links', 'inline')),
      ),
    );
  }

  // Only use a tableselect when the current user is able to perform any
  // operations.
  if ($admin_access) {
    $form['galleries'] = array(
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#empty' => t('No content available.'),
    );
  }
  // Otherwise, use a simple table.
  else {
    $form['galleries'] = array(
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $options,
      '#empty' => t('No content available.'),
    );
  }

  $form['pager']['#theme'] = 'pager';

  return $form;
}


/**
 * Validate gallery_admin_nodes form submissions.
 *
 * Check if any nodes have been selected to perform the chosen
 * 'Update option' on.
 */
function bo_gallery_galleries_page_admin_validate($form, &$form_state) {
  // Error if there are no items to select.
  if (!is_array($form_state['values']['galleries']) || !count(array_filter($form_state['values']['galleries']))) {
    form_set_error('', t('No items selectes.'));
  }
}


/**
 * Process gallery_admin_nodes form submissions.
 *
 * Execute the chosen 'Update option' on the selected nodes.
 */
function bo_gallery_galleries_page_admin_submit($form, &$form_state) {
  $operations = bo_gallery_operations();
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked nodes.
  $galleries = array_filter($form_state['values']['galleries']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($galleries), $operation['callback arguments']);
    }
    else {
      $args = array($galleries);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step. For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}


/**
 * Confirm page for gallery delete.
 */
function bo_gallery_multiple_delete_confirm($form, &$form_state, $galleries) {
  $form['galleries'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  // array_filter returns only elements with TRUE values.
  foreach ($galleries as $gid => $value) {
    $title = db_query('SELECT title FROM {bo_gallery} WHERE gid = :gid', array(':gid' => $gid))->fetchField();
    $form['galleries'][$gid] = array(
      '#type' => 'hidden',
      '#value' => $gid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) . "</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'bo_gallery_multiple_delete_confirm_submit';
  $confirm_question = format_plural(count($galleries),
                                  'Are you sure you want to delete this item?',
                                  'Are you sure you want to delete these items?');
  return confirm_form($form,
                    $confirm_question,
                    'admin/content/galleries', t('This action cannot be undone.'),
                    t('Delete'), t('Cancel'));
}


/**
 * Delete galleries.
 */
function bo_gallery_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    bo_gallery_delete_multiple(array_keys($form_state['values']['galleries']));
    $count = count($form_state['values']['galleries']);
    watchdog('gallery', 'Deleted @count posts.', array('@count' => $count));
    drupal_set_message(format_plural($count, 'Deleted 1 post.', 'Deleted @count posts.'));
  }
  $form_state['redirect'] = 'admin/content/galleries';
}


/**
 * Implements hook_bo_gallery_operations().
 */
function bo_gallery_operations() {
  $operations = array(
    'publish' => array(
      'label' => t('Publish selected gallery(s)'),
      'callback' => 'bo_gallery_mass_update',
      'callback arguments' => array('updates' => array('status' => 1)),
    ),
    'unpublish' => array(
      'label' => t('Unpublish selected gallery(s)'),
      'callback' => 'bo_gallery_mass_update',
      'callback arguments' => array('updates' => array('status' => 0)),
    ),
    'delete' => array(
      'label' => t('Delete selected gallery(s)'),
      'callback' => NULL,
    ),
  );
  return $operations;
}


/**
 * Function description.
 *
 * Make mass update of galleries, changing all galleries in the $galleries array
 * to update them with the field values in $updates.
 *
 * IMPORTANT NOTE: This function is intended to work when called
 * from a form submit handler. Calling it outgide of the form submission
 * process may not work correctly.
 *
 * @param array $galleries
 *   Array of gallery gids to update.
 * @param array $updates
 *   Array of key/value pairs with node field titles and the
 *   value to update that field to.
 */
function bo_gallery_mass_update($galleries, $updates) {
  // We use batch processing to prevent timeout when updating a large number
  // of nodes.
  if (count($galleries) > 10) {
    $batch = array(
      'operations' => array(
        array('_bo_gallery_mass_update_batch_process', array($nodes, $updates)),
      ),
      'finished' => '_bo_gallery_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'bo_gallery') . '/bo_gallery.pages.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($galleries as $gid) {
      _bo_gallery_mass_update_helper($gid, $updates);
    }
    drupal_set_message(t('The update has been performes.'));
  }
}

/**
 * Gallery Mass Update - helper function.
 */
function _bo_gallery_mass_update_helper($gid, $updates) {
  $gallery = bo_gallery_load($gid, NULL, TRUE);
  // For efficiency manually save the original node before applying any changes.
  $gallery->original = clone $gallery;
  foreach ($updates as $title => $value) {
    $gallery->$title = $value;
  }
  bo_gallery_save($gallery);
  return $gallery;
}


/**
 * Gallery Mass Update Batch operation.
 */
function _bo_gallery_mass_update_batch_process($galleries, $updates, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($galleries);
    $context['sandbox']['nodes'] = $galleries;
  }

  // Process nodes by groups of 5.
  $count = min(5, count($context['sandbox']['galleries']));
  for ($i = 1; $i <= $count; $i++) {
    // For each nid, load the node, reset the values, and save it.
    $gid = array_shift($context['sandbox']['galleries']);
    $gallery = _bo_gallery_mass_update_helper($gid, $updates);

    // Store result for post-processing in the finished callback.
    $context['results'][] = l($gallery->title, 'gallery/' . $gallery->gid);

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reaches.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}


/**
 * Node Mass Update Batch 'finished' callback.
 */
function _bo_gallery_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performes.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing gid not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $message .= theme('item_list', array('items' => $results));
    drupal_set_message($message);
  }
}


/**
 * Add new gallery.
 */
function bo_gallery_add() {
  global $user;

  $gallery = entity_get_controller('gallery')->create();
  $gallery->title = '';
  $gallery->status = 1;
  $gallery->created = REQUEST_TIME;
  $gallery->uid = $user->uid;
  drupal_set_title(t('Create @title', array('@title' => 'Gallery')), PASS_THROUGH);

  return drupal_get_form('bo_gallery_form', $gallery);
}


/**
 * Create gallery form.
 */
function bo_gallery_form($form, &$form_state, $gallery) {
  // Set the id to identify this as an gallery edit form.
  $form['#id'] = 'bo-gallery-form';

  // Save the gallery for later, in case we need it.
  $form['#bo_gallery'] = $gallery;
  $form_state['bo_gallery'] = $gallery;

  $user = new stdClass();
  if (!empty($gallery->uid)) {
    $user = user_load($gallery->uid);
  }
  else {
    $user->mail = '';
  }

  $form['options'] = array(
    '#type' => 'fieldset',
    '#access' => user_access('administer galleries'),
    '#title' => t('Publishing options'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#attributes' => array(
      'class' => array('node-form-options', 'bo-gallery-form'),
    ),
    '#attached' => array(
      'js' => array(drupal_get_path('module', 'node') . '/node.js'),
    ),
    '#weight' => -100,
    '#group' => 'bo_gallery_settings',
  );

  $form['bo_gallery_settings'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => 80,
  );

  $form['options']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Published'),
    '#default_value' => $gallery->status,
  );

  // Common fields. We don't have many.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $gallery->title,
    '#weight' => -5,
    '#required' => TRUE,
  );

  $form['uid'] = array('#type' => 'hidden', '#value' => $gallery->uid);

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('bo_gallery_form_submit'),
  );

  if (!empty($gallery->gid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access('delete galleries'),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('bo_gallery_form_delete_submit'),
    );
  }

  $form['#validate'][] = 'bo_gallery_form_validate';

  field_attach_form('gallery', $gallery, $form, $form_state);

  return $form;
}


/**
 * Gallery form valdation callback.
 *
 * Tell the Field system to do its own validation.
 */
function bo_gallery_form_validate($form, &$form_state) {
  // Field validation.
  entity_form_field_validate('gallery', $form, $form_state);
}


/**
 * Gallery form submit callback.
 */
function bo_gallery_form_submit($form, &$form_state) {
  global $user;

  $gallery = &$form_state['bo_gallery'];
  $account = $form['#bo_gallery'];

  $gallery->title = $form_state['values']['title'];
  $gallery->status = $form_state['values']['status'];
  $gallery->uid = $form_state['values']['uid'];
  $gallery->path = $form_state['values']['path'];

  // Notify field widgets.
  field_attach_submit('gallery', $gallery, $form, $form_state);

  // Save the gallery.
  $succ = bo_gallery_save($gallery);
  drupal_flush_all_caches();

  if ($succ) {
    // Notify the user.
    drupal_set_message(t('Gallery saved.'));
    $form_state['redirect'] = 'admin/content/galleries';
  }
  else {
    drupal_set_message(t('Gallery could not be saved. Please try again'), 'error');
    $form_state['redirect'] = 'admin/content/galleries/create';
  }
}


/**
 * Let the controller class do the hard work and save the gallery.
 */
function bo_gallery_save($gallery) {
  return entity_get_controller('gallery')->save($gallery);
}


/**
 * Function description.
 *
 * All this submit handler does is redirect the user
 * to gallery/$gid/delete. The rest of the code there
 * is simply to handle Drupal's page redirect system,
 * which we can largely copy and paste.
 */
function bo_gallery_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $gallery = $form['#bo_gallery'];
  $form_state['redirect'] = array('gallery/' . $gallery->gid . '/delete', array('query' => $destination));
}


/**
 * Function description.
 *
 * Rather than build a complete form, we will simply
 * pass data on to a utility function of the Form API
 * called confirm_form().
 */
function bo_gallery_delete_confirm($form, &$form_state, $gallery) {
  $form['#bo_gallery'] = $gallery;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['gid'] = array(
    '#type' => 'value',
    '#value' => $gallery->gid,
  );

  return confirm_form(
    $form,
    t('Are you sure you want to delete %title?', array('%title' => $gallery->title)),
    'gallery/' . $gallery->gid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}


/**
 * Function description.
 *
 * If the user submits the confirmation form, then
 * we know he really means it. The submit callback
 * handles deleting the gallery.
 */
function bo_gallery_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $gallery = bo_gallery_load($form_state['values']['gid']);
    bo_gallery_delete($gallery->gid);
    watchdog('gallery', '@type: deleted %title.', array('@type' => 'Gallery', '%title' => $gallery->title));
  }

  drupal_set_message(t('@type %title has been deleted.', array('@type' => 'Gallery', '%title' => $gallery->title)));

  $form_state['redirect'] = '<front>';
}


/**
 * Gallery edit page callback.
 */
function bo_gallery_page_edit($gallery) {
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => 'Gallery', '@title' => $gallery->title)), PASS_THROUGH);
  return drupal_get_form('bo_gallery_form', $gallery);
}
